// Le projet comprend les tâches suivantes :

// Créer une fonction retournant une liste des noms des acteurs.
// Créer une fonction retournant une liste des sorciers encore en vie.
// Créer une fonction retournant une liste des étudiants d'une maison spécifique (par exemple, "Gryffindor").
// Créer une fonction retournant une liste des personnages basée sur une recherche de nom incomplet (ex: "Weasley" renvoie 3 personnages).

// Utilisez les fonctions fléchées et les méthodes de tableaux map ou filter. Utilisez l'opérateur ternaire si possible.
// Bonus : Si ces tâches sont accomplies, développez un site web avec manipulation du DOM pour :

// Afficher la liste des personnages (compatible mobile et navigateur).
// Filtrer les personnages par maison ("Gryffindor", "Slytherin", "Hufflepuff" et "Ravenclaw")
// Rechercher des personnages par nom incomplet.

// Mega Bonus : Chargez la liste des personnages depuis l'API HP-API - all characters.

// Version avec fetch et API
const response = await fetch("https://hp-api.onrender.com/api/characters");
const characterList = await response.json();
//console.log(characterList );

// Créer une fonction retournant une liste des noms des acteurs.
const listActors = (list) =>
  list
    .filter((character) => character.actor) // condition pour vérifier que le champ n'est pas vide, revient à : != ""
    .map((character) => character.actor);

const listActorsHarryPotter = listActors(characterList);
//console.log(listActorsHarryPotter);

// Ancienne façon sans fetch et sans API
//import characterList from "./hp-characters.js";

//console.log(characterList[0]);

// Créer une fonction retournant une liste des sorciers encore en vie.
const listAliveCharacters = (listOfCharacters) => {
  const listOfCharactersFiltered = listOfCharacters.filter(
    (character) => character.alive
  );
  const listOfNames = listOfCharactersFiltered.map(
    (character) => character.name
  );
  console.log(listOfNames);
};
//listAliveCharacters(characterList);

// Créer une fonction retournant une liste des étudiants d'une maison spécifique (par exemple, "Gryffindor").
// Version humaine
/* const studentsOfHouse = (list, houseName) => {
  const studentsOfHouseFiltered = list.filter(
    (character) => character.house === `${houseName}`
  );
  const listOfNames = studentsOfHouseFiltered.map(
    (character) => character.name
  );
  console.log(listOfNames);
};
studentsOfHouse(characterList, "Gryffindor");
 */

// Version simplifiée

const studentsOfHouse = (list, houseName) =>
  list
    // Si la houseName du menuSelect === "toutes", alors on set à true tous les characters quels que soient leur character.house pour tout afficher
    // Sinon, on vérifie et on applique l'égalité character.house === houseName (value de l'option du menuSelect) pour ne garder que les characters de la maison sélectionnée
    .filter((character) =>
      houseName === "toutes" ? true : character.house === houseName
    )
    .map((character) => character.name);

let valeurSelectionnee;
let gryffindorStudents;

// Affichage dans le DOM
const containerStudentsHouse = document.querySelector(
  ".container-studentsHouse"
);

// Récupérer la référence de l'élément menu-select (form)
let menuSelectMaison = document.getElementById("maison");

// A l'événement 'change', au clic d'une option du menu select :
menuSelectMaison.addEventListener("change", function () {
  // Vider le contenu du container parent
  containerStudentsHouse.textContent = "";

  // Récupérer la valeur sélectionnée du menu select
  valeurSelectionnee = menuSelectMaison.value;
  
  // Déclencher la fonction studentsOfHouse, valeurSelectionnee = maison cliquée dans le form
  gryffindorStudents = studentsOfHouse(characterList, valeurSelectionnee);
  gryffindorStudents.sort();

  // Pour chaque student, créer un li dans le DOM, dans le container parent, et ajouter son contenu
  for (let student of gryffindorStudents) {
    const studentLi = document.createElement("li");
    studentLi.textContent = student;
    containerStudentsHouse.appendChild(studentLi);
  }
});

// Créer une fonction retournant une liste des personnages basée sur une recherche de nom incomplet (ex: "Weasley" renvoie 3 personnages).
const charactersSameName = (list, IncompleteName) =>
  list
    .filter((character) => character.name.includes(IncompleteName))
    .map((character) => character.name);

const listOfCharactersSameName = charactersSameName(characterList, "Weasley");
//console.log(listOfCharactersSameName);

// Simulation d'un "change" à l'affichage de la page sur le menuSelectMaison pour afficher par défaut toutes les maisons dès l'affichage de la page
menuSelectMaison.dispatchEvent(new Event("change"));
